<?php
require 'vendor/autoload.php';

use think\Template;

$config = require_once './config.php';
$viewConfig = $config['view'];

$template = new Template($viewConfig);
$navName = isset($_GET['name']) && $_GET['name'] != "" ? $_GET['name'] : "tools";
switch ($navName) {
    case "tools":
        $template->fetch('nav/index', ['menu' => $config['menu']['tools'],'action'=>'tools','name'=>'工具集']);
        break;
    case "doc":
        $template->fetch('nav/index', ['menu' => $config['menu']['doc'],'action'=>'doc','name'=>'开发文档']);
        break;
    case "site":
        $template->fetch('nav/index', ['menu' => $config['menu']['my_site'],'action'=>'site','name'=>'自有站点']);
        break;
    default:
        $template->fetch('nav/index', ['menu' => [],'action'=>'']);
}

