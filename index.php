<?php
// 设置模板引擎参数

require 'vendor/autoload.php';

use think\Template;
$config = require_once './config.php';
$viewConfig = $config['view'];

$template = new Template($viewConfig);
$template->fetch('index/index',['action'=>'index']);