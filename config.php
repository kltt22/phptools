<?php
/*

如果是 mysql5.4以下版本
在my.ini 文件中 设置 log='你的log 日志路径'  这个配置下面数组配置需要用得上
在[mysqld]后面增加一行  然后重启mysql 生效
log=D:/wamp/www/mysql_bz.log


* 如果是 mysql5.5以上版本  一次性修改方法 重启后无效
SHOW VARIABLES LIKE '%general_log%'
SET GLOBAL general_log = 1
SET GLOBAL general_log_file = '你的log 日志路径' 这个配置下面数组配置需要用得上


长期有效修改方法在 my.ini 里面 [mysqld] 后面加上如下代码 没有 [mysqld] 自己加上
[mysqld]
general_log=ON
general_log_file=D:/wamp/www/mysql_bz.log // 这里设置你 log日志路径  这个配置下面数组配置需要用得上
# log-raw=true  如果错误日志没记录 则开启这行, 参考地址 http://dev.mysql.com/doc/refman/5.7/en/query-log.html
# http://dev.mysql.com/doc/refman/5.7/en/password-logging.html
然后重启mysql 生效
错误的sql不会被成功解析，所以不会记录到general log中
如果需要记录所有的语句，包括那些错误的，请加 log-raw选项  log-raw=true
*/
return array(
	'mysql_log'=>'D:/web/mysql_bz.log', // mysql 标准日志文件路径
    'view'=>[
        'view_path'	    =>	'./template/',
        'cache_path'	=>	'./runtime/',
        'view_suffix'   =>	'html',
        'layout_on'     =>  true,
        'layout_name'   =>  'layout',
    ],
	'menu'=>[ // 菜单配置项
	    'tools'=>[//工具类
            '时间戳转换'=>'http://tool.chinaz.com/Tools/unixtime.aspx',
            'JSON格式化'=>'https://www.bejson.com/explore/index_new/',
            'phpinfo'=>'/phpinfo.php',
            'php探针'=>'/tz.php',
            '模拟提交'=>'http://ouapi.com/',
            'URLEncode'=>'http://tool.chinaz.com/Tools/URLEncode.aspx',
            '正则测试'=>'https://c.runoob.com/front-end/854',
        ],
	     'doc'=>[//文档
             'php手册'=>'http://www.w3school.com.cn/php/index.asp',
             '文库'=>'https://tool.lu/article',
             'bootstarp手册'=>'https://www.bootcss.com/',
             'TP3.2手册'=>'http://document.thinkphp.cn/manual_3_2.html',
             'TP5手册'=>'https://www.kancloud.cn/manual/thinkphp5',
             'TP5.1手册'=>'https://www.kancloud.cn/manual/thinkphp5_1/353946',
             'TP6手册'=>'https://www.kancloud.cn/manual/thinkphp6_0/1037479',
         ],
        'my_site'=>[//自有站点
            '自有文档'=>'http://www.w3school.com.cn/php/index.asp',
        ],
	],
);


